#!/bin/sh
###
# This program was written by and is copyright Alec Muffett 1991,
# 1992, 1993, 1994, 1995, and 1996, and is provided as part of the
# Crack v5.0 Password Cracking package.
#
# The copyright holder disclaims all responsibility or liability with
# respect to its usage or its effect upon hardware or computer
# systems, and maintains copyright as set out in the "LICENCE"
# document which accompanies distributions of Crack v5.0 and upwards.
#
# This is the Crack.makeonly version which *only* compiles the utilities
# under run/dir used for Crack
#
# This program was made based on Crack by Javier Fernández-Sanguino Peña
# for the Debian GNU/Linux distribution

###
# User-configurable junk for Crack
###

# nice, generic path (RedHat Linux, Solaris1, Solaris2)
CRACK_PATH=/usr/local/bin:/usr/ccs/bin:/usr/sbin:/sbin:/usr/bin:/bin:/usr/ucb:/usr/etc:$PATH

# compiler options for crack 5.0
#
# -DUSE_BZERO        /* add this to C5FLAGS if you don't have memset() */
# -DUSE_MALLOC_H
# -DUSE_PWD_H
# -DUSE_SIGNAL_H
# -DUSE_STDLIB_H
# -DUSE_STRINGS_H
# -DUSE_STRING_H
# -DUSE_SYS_TYPES_H
# -DUSE_UNISTD_H
# -DMAXWORDLEN=      /* ignore if you don't read the code */
# -DNUMWORDS=        /* ignore if you don't read the code */
# -DSTRINGSIZE=      /* ignore if you don't read the code */

# this set tested on:
# - solaris 2.5
# - redhat linux 4.0
# - digital unix v4.0

C5FLAGS="-DUSE_STRING_H -DUSE_STDLIB_H -DUSE_SIGNAL_H -DUSE_SYS_TYPES_H -DUSE_UNISTD_H -DUSE_PWD_H $(dpkg-buildflags --get CFLAGS)"

#
# now pick your compiler
#

# vanilla unix cc
#CC=cc
#CFLAGS="-g -O2 $C5FLAGS"
#LIBS=-lcrypt # uncomment only if necessary to use stdlib crypt(), eg: NetBSD MD5

# gcc 2.7.2
: "${CC:=gcc}"
CFLAGS="-g -O2 -Wall -fstack-protector-strong -Wformat -Werror=format-security $C5FLAGS"
LIBS=-lcrypt # uncomment only if necessary to use stdlib crypt(), eg: NetBSD MD5

# digital unix v4.0, CFLAGS for ev4/ev5 chipsets (pick one)
#CC=cc
#CFLAGS="-O4 -fast -tune ev4 $C5FLAGS"
#CFLAGS="-O4 -fast -tune ev5 $C5FLAGS"
#LIBS=

# Uncomment the next two lines if a) you are running Crack in
# networking mode, and b) your environment's operating system will not
# necessarily run binaries compiled on other revs of the same
# operating system which have the same architecture and the same
# *MAJOR* revision number.
#
# eg: if you have Foonix 2.0 boxes as well as Foonix 2.1 boxes; in
# this example, the major revision number is "2".  If the Foonix 2.1
# O/S will not run Foonix 2.0 binaries for some reason, or vice-versa,
# then uncomment these lines.

#STRICT_OSREV=yes
#export STRICT_OSREV

# Uncomment and/or modify on HP/UX or similar where the UCB "rsh"
# command has been renamed.  See scripts/crack-rsh also
#CRACK_RSH=remsh
#CRACK_RCP=rcp

###########################################################
###########################################################
#################### THAT'S ALL, FOLKS ####################
#### NOW GO CONFIGURE YOUR CRYPT ALGORITHM, EG: LIBDES ####
###########################################################
###########################################################

###
# security
###

umask 077
export CRACK_PATH                       # So it can be picked up later
PATH=$CRACK_PATH                        # For temporary use...
export PATH

###
# Defaults
###

usage="Usage: Crack [options] [bindir] [[-fmt format] files]..."

version="5.0a"                          # version string
deffmt=trad                             # for default trad2spf
dodie=""                                # for verbose usage/die
debug="false"                           # ...guess...
node=`uname -n`                         # more portable then `hostname`

###
# home base
###

CRACK_HOME=.

if cd $CRACK_HOME
then
	CRACK_HOME=`pwd`
else
	echo "Fatal Error: $CRACK_HOME: cannot chdir" 1>&2
	exit 1
fi

export CRACK_HOME

###
# sanity check
###

if [ ! -f "$CRACK_HOME/Crack" ]
then
	echo "Fatal Error: $CRACK_HOME: something weird going down" 1>&2
	exit 1
fi

###
# Hierarchy
###

for dir in lib lib/bin
do
	test -d $dir || mkdir $dir || exit 1
done

###
# Flagwaving
###

echo "Crack $version: The Password Cracker."
echo "(c) Alec Muffett, 1991, 1992, 1993, 1994, 1995, 1996"
echo "System:" `uname -a 2>/dev/null`
echo "Home: $CRACK_HOME"
echo "Invoked: $0 $*"


###
# Sanity test bindir - written in gruntscript for clarity.
###

bdname=`scripts/binstamp 2>/dev/null`

if [ "x$bdname" = "x" ]
then
	echo "Crack: error: cannot continue as binstamp returned empty string" 1>&2
	exit 0
fi

echo "Stamp: $bdname"
echo ""

bindir=lib/bin/$bdname


###
# Reset PATH in advance
###

PATH=$CRACK_HOME/scripts:$CRACK_HOME/$bindir:$PATH
export PATH

###
# Make the binaries
###

echo "Crack: making utilities in $bindir"

if [ ! -d $bindir ] # small redundancy, big benefit
then
	mkdir $bindir || exit 1
fi

make clean || exit 1

make ARGS="\"XDIR=../../$bindir\" \"XCFLAGS=$CFLAGS\" \"XCC=$CC\" \"XLIBS=$LIBS\"" utils || exit 1

###
# Make Only ?
###

if [ "x$makeonlyflag" != "x" ]
then
	echo "Crack: makeonly done"
	exit 0
fi

exit 0
